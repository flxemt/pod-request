const form = document.getElementById('form')
const emailError = document.getElementById('email-error')

function handleSubmit(event) {
  event.preventDefault()

  const formData = new FormData(form)
  const email = formData.get('email')
  const isEmailMatch = email.match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)

  emailError.style.display = isEmailMatch ? '' : 'block'
  if (!isEmailMatch) return
}

form.addEventListener('submit', handleSubmit)
